# Windows Manager for Google Chrome
**NOTE:** Chrome<sup>TM</sup> is a trademark of Google Inc. Use of this trademark is subject to Google Permissions.  

This Chrome<sup>TM</sup> browser extension provides two windows management functions:
- Tiling windows.
- Emulating workspaces


**Why not just use the new native Virtual Desktops functionality of ChromeOS?**  
There are still 3 pending features for ChromeOS native implementation to reach feature parity with this extension.
- [crbug.com/996999](https://bugs.chromium.org/p/chromium/issues/detail?id=996999): Restore windows to existing virtual desktops.
- [crbug.com/1023794](https://bugs.chromium.org/p/chromium/issues/detail?id=1023794): Make Window visible in all Virtual Desks.
- [crbug.com/850681](https://bugs.chromium.org/p/chromium/issues/detail?id=850681): Add additional window tiling drag targets: Top, Bottom, Corners.  
  

## Features
- Multi-monitor support.
- Emulates up to 9 workspaces.
- Tiles windows to 8 different positions. (4 corners + 4 split screen)
- Remembers the windows workspace assignment between system reboots.
- Allows to define custom keyboard shortcuts.
- Multi-platform support: ChromeOS, Linux, Mac and Windows.

### Required Permissions
- **Display Notifications**: To Notify when switching to another workspace.
- **Read your browsing history**: To recover each window's workspace assignment
after a reboot or browser restart.  
It creates a unique hash number for each window based on the amount of tabs open and the url of the last tab, and this permission is needed in order to get that information.


## Use Guide
### Install and Configure
1. Install from the [Chrome<sup>TM</sup> Webstore](https://chrome.google.com/webstore/detail/chrome-windows-manager/gophpkegccafhjahoijdembdkbjpiflb)
2. Click on the extension icon on the toolbar.
3. Click on the 'Configure shortcuts' button.
4. Configure the keyboard shortcuts to switch workspaces. (I recommend using `Alt+X` and `Alt+Z`)

### Tile Windows
  With the Chrome<sup>TM</sup> Browser window in focus, use the configured keyboard shortcuts to tile it to different positions.  
  ![](https://gitlab.com/EduCampi/chromewm/raw/master/docs/chromewm-tiling.gif)
  
### Switch Workspaces:
  1. Press the *Next Workspace* or *Previous Workspace* keyboard shortcut.
  2. A notification will be displayed indicating which workspace is now active.  
  NOTE: The extension icon will always display the active workspace.  
  ![](https://gitlab.com/EduCampi/chromewm/raw/master/docs/chromewm-workspaces.gif)
  
### Move a window to another workspace:
  1. Switch to the destination workspace.
  2. Bring the window to focus using `Alt+Tab`, or another window management
  function of the native Operative System.
  
## Known Limitations
- Does not recover the workspaces state if the browser crashes due to a power
loss or the process being terminated. (See [issue #1](https://gitlab.com/EduCampi/chromewm/issues/1))
- Cannot set all shortcuts by default due to an imposed limit of 4 by Chrome.
- It will automatically open a new Chrome™ window when switching to an empty workspace.
That's necessay in order for Chrome™ to be in focus and listen for keyboard shortcuts.
(The window will be removed if it's not used when switching workspaces)


## Building the extension
I did this cause there are no workspaces in ChromeOS (yet), I'm [not a developer](https://xkcd.com/1513) but I'll try to keep improving the code as I learn Javascript.

### How to:
1. You will need to [install Bazel](https://docs.bazel.build/versions/master/install.html).
2. Clone the repository:  
`git clone https://gitlab.com/EduCampi/chromewm.git`

3. Build the extension:  
`cd chromewm`  
`bazel build extension`
4. The built extension will be at `bazel-genfiles/extension.zip`.