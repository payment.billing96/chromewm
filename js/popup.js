/**
 * @fileoverview PopUp file
 * @author EduCampi
*/
goog.provide('chromewm.popup');

goog.require('goog.dom');
goog.require('goog.events');


goog.events.listenOnce(
    goog.dom.getDocument(),
    goog.events.EventType.DOMCONTENTLOADED,
    () => {
      /** @type {!chromewm.popup} popupPage */
      const popupPage = new chromewm.popup();
      popupPage.init();
    }
);


/**
 * Contructor for the Options Object
 * @constructor @export
 */
chromewm.popup = function() {
};


/**
 * Initializes properties and listeners
 * @export
 */
chromewm.popup.prototype.init = function() {

  chrome.windows.getCurrent({'windowTypes': ['normal']},
    (currentWindow) => {
      chrome.runtime.sendMessage(
          {'message': "pinStatus",
           'windowId': currentWindow['id']
          },
          (/** @type {boolean} */ response) => {
            if (response) {
              const pinSwitch = goog.dom.getElement('pinWindow');
              pinSwitch.parentElement['MaterialSwitch']['on']();
            }
          }
      );

      goog.events.listen(
          goog.dom.getElement('pinWindow'),
          goog.events.EventType.CHANGE,
          () => {
            chrome.runtime.sendMessage(
                {'message': "pinWindow",
                 'windowId': currentWindow['id']
            });
          }
      );
  });

  goog.events.listen(
      goog.dom.getElement('showOptions'),
      goog.events.EventType.CLICK,
      () => {
        chrome.runtime.openOptionsPage();
      }
  );
};